# JetBrains Fleet for Space Configuration

If you chose to use JetBrains Fleet through JetBrains Space instead of Gitpod Code in the browser (or VS Code with the Gitpod SSH extension), we setup

## The Custom Dockerfile

Source code for the `quay.io/gitpodified-workspace-images/recaptime-dev-envrionment` base image can be found at
<https://gitlab.com/RecapTime/infra/dev-environments-docker>. The custom Dockerfile in this directory is also the one we use
for Gitpod and VS Code Remote Containers to keep things in one place.

## Setup for contributors

Don't like this configuration hell for your personal JetBrains Space? Just use Gitpod instead.

1. [Sign up for JetBrains Space](https://www.jetbrains.com/space/?utm_source=jetbrains.space&utm_medium=referral) using your personal email,
and complete setup, excluding adding team members.
2. If you create an new project during the onboarding wizard.

## Setup for squad members

1. [Sign in](https://recaptime.jetbrains.space/sign-in) using your `recaptime.tk` domain.
2. Navigate to <https://recaptime.jetbrains.space/projects/RTAPP-DOCS> an select `Dev Environment`.
